var firebase = require("firebase");

const firebaseConfig = {
    apiKey: "AIzaSyBxCe40i1-dutsweY9nrhX695a76Pe8woQ",
    authDomain: "iot-bb018.firebaseapp.com",
    databaseURL: "https://iot-bb018-default-rtdb.firebaseio.com",
    projectId: "iot-bb018",
    storageBucket: "iot-bb018.appspot.com",
    messagingSenderId: "920045867116",
    appId: "1:920045867116:web:26554dd2eed55fd09ac849",
    measurementId: "G-30N1D4F16Y"
  };

  var MyFirebase = firebase.initializeApp(firebaseConfig);
  

  module.exports =  MyFirebase;